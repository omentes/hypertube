# Hypertube

Study project at UNIT Factory, Kyiv (Ecole 42 / School 42)
This project proposes to create a web application that allows the user to research and watch videos.

## DESCRIPTION

The player will be directly integrated to the site, and the videos will be downloaded through the BitTorrent protocol. The research engine will interrogate multiple external sources of your choice, like for example http://www.legittorrents.info, or even https://archive.org. Once the element selected, it will be downloaded from the server and streamed on the web player at the same time. Which means that the player won’t only show the video once the download is completed, but will be able to stream directly the video feed.

## INSTALL

```
git clone https://gitlab.com/omentes/hypertube.git

```

## ACCESS

frontend:
```
http://127.0.0.1/
```

backend:
```
http://127.0.0.1/
```

MongoDB:
```
https://mlab.com/
```

## Frontend TODO
* one
* two
* three

## Backend TODO
* create Restful API for manage users 
* add authorisation with Oauth2  

## Methods from API
Please, read README.md at [back directory](https://gitlab.com/omentes/hypertube/blob/master/back/)

## Built With

* NodeJS
* ReactJS
* RESTful API

## Authors

*  **Artem Pakhomov** - [apakhomo](https://gitlab.com/omentes/) - apakhomo@student.unit.ua
*  **Mike Vaskiv** - [mvaskiv](https://gitlab.com/mvaskiv/) - mvaskiv@student.unit.ua

## License

This project is licensed under the MIT License
