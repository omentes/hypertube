# Methods from API

Methods and routes for data, backend Hypertube

## Users

```
--------------------------
FIELD        => VALUE
--------------------------
_id          => Object id
username     => string
firstname    => string
lastname     => string
email        => string
password     => wh hash
--------------------------
...
-------------------------------------------------------------------------------------------------------------
HOW     =>  URL                        =>  DESCRIPTION                                        RETURN
-------------------------------------------------------------------------------------------------------------
POST    =>  /users/new                 =>  create new user                                    user-object
GET     =>  /users/:id                 =>  get data from users by id.                         user-object
GET     =>  /users/email/:email        =>  get data from users by email (for validation)      user-object
GET     =>  /users/username/:username  =>  get data from users by username (for validation)   user-object
PUT     =>  /users/:id                 =>  update user by id                                  user-object
DELETE  =>  /users/:id                 =>  delete user by id                                  _id
-------------------------------------------------------------------------------------------------------------
For POST need all varaibles! Password converting to hash on saving to db.
For PUT need all varaibles! If user change password - need send whirpool hash password!
GET && PUT return all varaibles! Save it in coockie or another place
```

## Auth

```
HOW     =>  URL                        =>  DESCRIPTION                                        RETURN
-------------------------------------------------------------------------------------------------------------
POST    =>  /auth/new                  =>  create new session                                 return object
```

## Built With

* NodeJS
* RESTful API

## Authors

*  **Artem Pakhomov** - [apakhomo](https://gitlab.com/omentes/) - apakhomo@student.unit.ua
