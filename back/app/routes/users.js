var ObjectID = require('mongodb').ObjectID;
var  crypto         = require('crypto');


module.exports = function(app, db) {
app.post('/users/new', (req, res) => {
    if (req.body.username && req.body.username !== 'undefined') {
      if (req.body.firstname && req.body.firstname !== 'undefined') {
        if (req.body.lastname && req.body.lastname !== 'undefined') {
          if (req.body.email && req.body.email !== 'undefined') {
            if (req.body.password && req.body.password !== 'undefined') {
              var hash = crypto.createHash('whirlpool');
              data = hash.update(req.body.password, 'utf-8');
              genPasswordHash= data.digest('hex');
              // Printing the output on the console
              // console.log("username  : " + req.body.username);
              // console.log("firstname : " + req.body.firstname);
              // console.log("lastname  : " + req.body.lastname);
              // console.log("email     : " + req.body.email);
              // console.log("hash.     : " + genPasswordHash);
              const note = {
                username: req.body.username,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: genPasswordHash,
              };
              db.collection('users').insert(note, (err, result) => {
                if (err) { 
                  res.send({ 'error': 'An error has occurred' }); 
                } else {
                  res.send(result.ops[0]);
                }
              });
            }
          }
        }
      }
    }
  });
  app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    db.collection('users').findOne(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'});
      } else {
        console.log(item);
        res.send(item);
      }
    });
  });
  app.get('/users/email/:email', (req, res) => {
    const email = req.params.email;
    const details = { 'email': email};
    db.collection('users').findOne(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'});
      } else {
        console.log(item);
        res.send(item);
      }
    });
  });
  app.get('/users/username/:username', (req, res) => {
    const username = req.params.username;
    const details = { 'username': username};
    db.collection('users').findOne(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'});
      } else {
        console.log(item);
        res.send(item);
      }
    });
  });
  app.put ('/users/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    const note = {
      username: req.body.username,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      password: req.body.password,
    };
    db.collection('users').update(details, note, (err, result) => {
      if (err) {
          res.send({'error':'An error has occurred'});
      } else {
          res.send(note);
      }
    });
  });
  app.delete('/users/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    db.collection('users').remove(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'});
      } else {
        res.send(id);
      }
    });
  });
};